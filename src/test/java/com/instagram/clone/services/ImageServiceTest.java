package com.instagram.clone.services;

import com.instagram.clone.entity.ImageMetadata;
import com.instagram.clone.entity.repo.ImageMetadataRepository;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {

  @Mock
  private FileStorageService fileStorageService;
  @Mock
  private ImageMetadataRepository imageMetadataRepository;

  private ImageService imageService;

  @BeforeEach
  public void beforeEach() {
    imageService = new ImageService(fileStorageService, imageMetadataRepository);
  }

  @Test
  void givenFile_testShouldVerifyTheFileUploadedSuccessfullyAndTheFileMetadataIsReturned() {
    ImageMetadata imageMetadata = ImageMetadata.builder().createdAt(Instant.now()).fileName("sample.jpeg")
            .fileType("jpeg").uri("/home/resources/").username("some user").build();
    when(fileStorageService.store(any(), anyString())).thenReturn(imageMetadata);
    when(imageMetadataRepository.save(any())).thenReturn(imageMetadata);

    FileItemFactory fileItemFactory = new DiskFileItemFactory();
    FileItem fileItem = fileItemFactory.createItem("test", "test", true, "test");
    CommonsMultipartFile multipartFile = new CommonsMultipartFile(fileItem);
    ImageMetadata uploadedImageMetadata = imageService.upload(multipartFile, "some user");
    assertThat(uploadedImageMetadata, notNullValue());
  }
}
