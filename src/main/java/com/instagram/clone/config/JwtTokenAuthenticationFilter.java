package com.instagram.clone.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

  private final JwtConfig jwtConfig;

  public JwtTokenAuthenticationFilter(final JwtConfig jwtConfig) {
    this.jwtConfig = jwtConfig;
  }

  @Override
  protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                  final FilterChain filterChain) throws ServletException, IOException {

    String header = request.getHeader(jwtConfig.getHeader());
    if (header == null || !header.startsWith(jwtConfig.getPrefix())) {
      filterChain.doFilter(request, response);
      return;
    }
    final String token = header.replace(jwtConfig.getPrefix(), "");
    try {
      parseToken(token);
    } catch (Exception exception) {
      SecurityContextHolder.clearContext();
    }
    filterChain.doFilter(request, response);
  }

  private void parseToken(final String token) throws Exception {
    Claims claims = Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes()).parseClaimsJws(token).getBody();
    final String userName = claims.getSubject();
    if (userName != null) {
      List<String> authorities = (List<String>) claims.get("authorities");
      var authToken = new UsernamePasswordAuthenticationToken(userName, null, authorities.stream()
              .map(SimpleGrantedAuthority::new).collect(toList()));
      SecurityContextHolder.getContext().setAuthentication(authToken);
    }
  }
}
