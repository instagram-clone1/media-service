package com.instagram.clone.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class JwtConfig {

  private final String authUrl;
  private final String header;
  private final String prefix;
  private final int expiration;
  private final String secret;

  public JwtConfig(@Value("${security.jwt.uri}") final String authUrl,
                   @Value("${security.jwt.header}") final String header,
                   @Value("${security.jwt.prefix}") final String prefix,
                   @Value("${security.jwt.expiration}") final int expiration,
                   @Value("${security.jwt.secret}") final String secret) {
    this.authUrl = authUrl;
    this.header = header;
    this.prefix = prefix;
    this.expiration = expiration;
    this.secret = secret;
  }
}
