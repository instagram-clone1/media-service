package com.instagram.clone.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class StaticResourceConfiguration implements WebMvcConfigurer {

  private final String uploadDirectory;
  private final String mediaFilePath;

  public StaticResourceConfiguration(@Value("${file.upload-dir}") final String uploadDirectory,
                                     @Value("${file.path.prefix}") final String mediaFilePath) {
    this.uploadDirectory = uploadDirectory;
    this.mediaFilePath = mediaFilePath;
  }

  @Override
  public void addResourceHandlers(final ResourceHandlerRegistry resourceHandlerRegistry) {
    resourceHandlerRegistry.addResourceHandler(mediaFilePath + "/**")
            .addResourceLocations("file:" + uploadDirectory);
  }
}
