package com.instagram.clone.services;

import com.instagram.clone.entity.ImageMetadata;
import com.instagram.clone.entity.repo.ImageMetadataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class ImageService {

  private final FileStorageService fileStorageService;
  private final ImageMetadataRepository imageMetadataRepository;

  public ImageService(final FileStorageService fileStorageService,
                      final ImageMetadataRepository imageMetadataRepository) {
    this.fileStorageService = fileStorageService;
    this.imageMetadataRepository = imageMetadataRepository;
  }

  public ImageMetadata upload(final MultipartFile multipartFile, final String userName) {
    ImageMetadata imageMetadata = fileStorageService.store(multipartFile, userName);
    return imageMetadataRepository.save(imageMetadata);
  }
}
