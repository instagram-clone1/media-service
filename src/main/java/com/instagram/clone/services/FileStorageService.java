package com.instagram.clone.services;

import com.instagram.clone.entity.ImageMetadata;
import com.instagram.clone.services.exception.InvalidFileException;
import com.instagram.clone.services.exception.InvalidFileNameException;
import com.instagram.clone.services.exception.StorageException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Slf4j
@Service
public class FileStorageService {

  private final String uploadDirectory;
  private final String mediaFilePath;

  private final Environment environment;

  public FileStorageService(@Value("${file.upload-dir}") final String uploadDirectory,
                            @Value("${file.path.prefix}") final String mediaFilePath,
                            final Environment environment) {
    this.uploadDirectory = uploadDirectory;
    this.mediaFilePath = mediaFilePath;
    this.environment = environment;
  }

  public ImageMetadata store(final MultipartFile multipartFile, final String userName) {
    final String fileName = getFileNameOrThrowExceptionOnInvalidFile(multipartFile);
    log.info("storing file {}", fileName);
    final String extension = FilenameUtils.getExtension(fileName);
    final String newFileName = String.join(".", UUID.randomUUID().toString(), extension);
    try (InputStream inputStream = multipartFile.getInputStream()) {
      Path userDir = Paths.get(uploadDirectory + userName);
      if (Files.notExists(userDir)) {
        Files.createDirectories(userDir);
      }
      Files.copy(inputStream, userDir.resolve(newFileName), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException exception) {
      log.error("Error reading the file");
      throw new InvalidFileException("failed to store empty file " + fileName);
    }
    return createImageMetaData(userName, newFileName, multipartFile);
  }

  private ImageMetadata createImageMetaData(final String userName, final String newFileName,
                                                      final MultipartFile multipartFile) {
    final String port = environment.getProperty("local.server.port");
    try {
      String hostName = InetAddress.getLocalHost().getHostName();
      String fileUrl = String.format("http://%s:%s%s/%s/%s", hostName, port, mediaFilePath, userName, newFileName);
      return ImageMetadata.builder().fileName(newFileName).uri(fileUrl).fileType(multipartFile.getContentType())
              .username(userName).build();
    } catch (UnknownHostException exception) {
      log.error("Unable to recognized the host");
      throw new StorageException("Failed to store file " + multipartFile.getOriginalFilename(), exception);
    }
  }

  private String getFileNameOrThrowExceptionOnInvalidFile(final MultipartFile multipartFile) {
    final String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
    if (multipartFile.isEmpty()) {
      log.error("could not store empty file {}", fileName);
      throw new InvalidFileException("failed to store empty file " + fileName);
    }
    if (fileName.contains("..")) {
      log.warn("cannot store file with relative path {}", fileName);
      throw new InvalidFileNameException("Cannot store file with relative path outside current directory " + fileName);
    }
    return fileName;
  }
}
