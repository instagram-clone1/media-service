package com.instagram.clone.services.exception;

public class InvalidFileException extends RuntimeException {

  public InvalidFileException(final String errorMessage) {
    super(errorMessage);
  }
}
