package com.instagram.clone.services.exception;

public class StorageException extends RuntimeException {

  public StorageException(final String errorMessage, final Throwable cause) {
    super(errorMessage, cause);
  }
}
