package com.instagram.clone.services.exception;

public class InvalidFileNameException extends RuntimeException {

  public InvalidFileNameException(final String errorMessage) {
    super(errorMessage);
  }
}
