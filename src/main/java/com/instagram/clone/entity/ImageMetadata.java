package com.instagram.clone.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Getter
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class ImageMetadata {

  @Id
  private String id;
  @CreatedBy
  private String username;
  @CreatedDate
  private Instant createdAt;
  @NonNull
  private String fileName;
  @NonNull
  private String uri;
  @NonNull
  private String fileType;
}

