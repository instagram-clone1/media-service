package com.instagram.clone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@EnableEurekaClient
@EnableMongoAuditing
@SpringBootApplication
public class InstagramMediaServiceApplication {

  public static void main(final String[] args) {
    SpringApplication.run(InstagramMediaServiceApplication.class, args);
  }
}
